import { Observable } from "rxjs/Observable";
import { HttpErrorResponse } from "@angular/common/http";

export class ErrorHendler {

    static handlerErro(error: HttpErrorResponse | any) {
        let errorMessage : string;

        if(error instanceof HttpErrorResponse) {
            const body = error.error;
            errorMessage = `Erro ${error.status} ao acessar a URL ${error.url} - ${body}`;
        } else {
            errorMessage = error.errorMessage ? error.errorMessage : error.toString();
        }
        console.log(errorMessage)
        return Observable.throw(errorMessage);
    } 
}