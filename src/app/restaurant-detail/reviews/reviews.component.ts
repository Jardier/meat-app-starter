import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RestaurantsService } from 'app/restaurants/restaurants.service';
import { Review } from 'app/shared/review.model';

@Component({
  selector: 'mt-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {

  reviews : Observable<Review>;

  constructor(private restaurantService : RestaurantsService ,
              private router : ActivatedRoute
    ) { }

  ngOnInit() {
    this.reviews = this.restaurantService
                       .reviewsOfRestaurant(this.router.parent.snapshot.params['id']);
                        
  }

}
