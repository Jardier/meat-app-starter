import { Component, OnInit } from '@angular/core';
import { RestaurantsService } from 'app/restaurants/restaurants.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { MenuItem } from 'app/shared/menu-item.model';

@Component({
  selector: 'mt-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public menuItem : Observable<MenuItem[]>

  constructor(
    private restaurantsService : RestaurantsService,
    private router : ActivatedRoute
  ) { }

  ngOnInit() {
    this.menuItem = this.restaurantsService
                        .menuOfRestaurant(this.router.parent.snapshot.params['id']);
  }

  public addMenuItem(menuItem: MenuItem) {
    console.log("Item: ", menuItem);
  }

}
