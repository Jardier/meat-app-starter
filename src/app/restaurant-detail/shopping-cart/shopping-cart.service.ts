import { MenuItem } from "app/shared/menu-item.model";
import { CartItem } from "app/shared/cart-item.model";
import { Injectable } from "@angular/core";
import { NotificationService } from "app/shared/messages/notification.service";

@Injectable()
export class ShoppingCartService {
    public items : CartItem[] = [];

    constructor(
        private notificationService : NotificationService
    ){}

    public clear() {
        this.items = [];
    }

    public addItem(item : MenuItem) {
        let foundItem = this.items.find((cardItem) => cardItem.menuItem.id === item.id);

        if(foundItem) {
            this.increaseQty(foundItem);
        } else {
            this.items.push(new CartItem(item));
        }
        this.notificationService.notify(`Você adicionou o item ${item.name}`);
    }

    public removeItem(item: CartItem) {
        this.items.splice(this.items.indexOf(item), 1);
        this.notificationService.notify(`Você removeu o item ${item.menuItem.name}`);
    }

    public total() : number {
       return this.items
                  .map(item => item.value())
                  .reduce((prev, value) => prev + value, 0);
    }

    public increaseQty(item : CartItem) {
        item.quantity += 1;
    }

    public decreadeQty(item: CartItem) {
        item.quantity -=1;
        if(item.quantity === 0) {
            this.removeItem(item);
        }
    }
}