import { Component, OnInit } from '@angular/core';
import { RadioOption } from 'app/shared/radio/radio-option.model';
import { CartItem } from 'app/shared/cart-item.model';
import { OrderCompraService } from './order-compra.service';
import { Order, OrderItem } from 'app/shared/order.model';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'mt-order-compra',
  templateUrl: './order-compra.component.html',
  styleUrls: ['./order-compra.component.css']
})
export class OrderCompraComponent implements OnInit {

  public orderForm : FormGroup;

  public delivery : number = 8;

  public paymentOptions : RadioOption [] = [
    { label: 'Dinheiro', value: 'MON' } ,
    { label: 'Cartão de Débito', value: 'DEB' },
    { label: 'Cartão Refeição', value: 'REF' }
  ]

  private emailPattern = /^[a-z0-9.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i;
  private numberPattern = /^[0-9]{1,}$/;

  constructor(
    private orderService: OrderCompraService,
    private router : Router,
    private formBuilder : FormBuilder
    ) {}

  ngOnInit() {
    this.orderForm = this.formBuilder.group({
      name: this.formBuilder.control('' , [Validators.required, Validators.minLength(5)]),
      email: this.formBuilder.control('', [Validators.required, Validators.pattern(this.emailPattern)]),
      emailConfirmation: this.formBuilder.control('', [Validators.required, Validators.pattern(this.emailPattern)]),
      address: this.formBuilder.control('', [Validators.required, Validators.minLength(5)]),
      number : this.formBuilder.control('', [Validators.required, Validators.pattern(this.numberPattern)]),
      optionalAddress: this.formBuilder.control(''),
      paymentOption: this.formBuilder.control('',[Validators.required])
    }, {validator : OrderCompraComponent.equalsTo})
  }

  public cartItems() : CartItem[] {
    return this.orderService.cartItems();
  }

  public increaseQty(item: CartItem) {
    this.orderService.increaseQty(item);
  }

  public decreaseQty(item: CartItem) {
    this.orderService.decreadeQty(item);
  }

  public remove(item: CartItem) {
    this.orderService.remove(item);
  }

  public itemsValue() : number {
    return this.orderService.itemsValue();
  }

  public checkOrder(order : Order) : void {
    order.orderItem = this.cartItems()
                          .map((item: CartItem) => 
                          new OrderItem(item.quantity, item.menuItem.id));
    this.orderService.checkOrder(order).subscribe((id : string) => {
      console.log(`Ordem de compra número: ${id}`);
      this.router.navigate(['/order-summary']);
      this.orderService.clear();
    });
    
  }
  
  static equalsTo(group : AbstractControl) : {[key:string]:boolean} {
    const email = group.get('email');
    const emailConfirmation = group.get('emailConfirmation');

    if(!email || !emailConfirmation) {
      return undefined;
    }
    if(email.value !== emailConfirmation.value) {
      return {emailsNotMatch:true};
    }

    return undefined;

  }
}
