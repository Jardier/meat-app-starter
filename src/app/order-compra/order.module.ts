import { NgModule } from "@angular/core";
import { SharedModule } from "app/shared/shared.module";
import { Routes, RouterModule } from "@angular/router";

import { OrderCompraComponent } from "./order-compra.component";
import { DeliveryCostsComponent } from "./delivery-costs/delivery-costs.component";
import { OrderItemsComponent } from "./order-items/order-items.component";

const ROUTES : Routes = [
    {path: 'order', component: OrderCompraComponent}
]

@NgModule({
    declarations : [OrderCompraComponent, DeliveryCostsComponent, OrderItemsComponent],
    imports : [SharedModule, RouterModule.forChild(ROUTES)],
})
class OrderModule {

}
export { OrderModule }