import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { CartItem } from 'app/shared/cart-item.model';

@Component({
  selector: 'mt-order-items',
  templateUrl: './order-items.component.html',
  styleUrls: ['./order-items.component.css']
})
export class OrderItemsComponent implements OnInit {

  @Input()
  public items : CartItem[];

  @Output()
  public increaseQty = new EventEmitter<CartItem>();
  @Output()
  public decreaseQty = new EventEmitter<CartItem>();
  @Output()
  public remove = new EventEmitter<CartItem>();


  constructor() { }

  ngOnInit() {
  }

  public emitIncreaseQty(item : CartItem) : void {
    this.increaseQty.emit(item);
  }

  public emitDecreaseQty(item : CartItem) : void {
    this.decreaseQty.emit(item);
  }

  public emitRemove(item : CartItem) : void {
    this.remove.emit(item);
  }
}
