import { Component, OnInit } from '@angular/core';
import { OrderCompraService } from '../order-compra.service';
import { Router } from '@angular/router';

@Component({
  selector: 'mt-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.css']
})
export class OrderSummaryComponent implements OnInit {

  public orderId : string;

  public rated : boolean;

  constructor(
      private orderService: OrderCompraService,
      private router : Router,
    
    ) {}

  ngOnInit() {
    this.orderId = this.orderService.orderId;
    
    if(this.orderId === undefined) {
      // this.router.navigate(['/']);
    }
  }
  public rate() {
    this.rated = true;
  }

}
