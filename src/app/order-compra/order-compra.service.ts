import { Injectable, OnInit } from "@angular/core";
import { ShoppingCartService } from "app/restaurant-detail/shopping-cart/shopping-cart.service";
import { CartItem } from "app/shared/cart-item.model";
import { Order } from "app/shared/order.model";
import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";
import { MEAT_API } from "app/app.api";


@Injectable()
export class OrderCompraService implements OnInit {

    public orderId : string;

    constructor(
        private shoppingService : ShoppingCartService,
        private http: HttpClient
        ){}

    ngOnInit() {
        
    }

   public cartItems() : CartItem[] {
       return this.shoppingService.items;
   }

   public increaseQty(item : CartItem) {
       this.shoppingService.increaseQty(item);
   } 

   public decreadeQty(item : CartItem) {
       this.shoppingService.decreadeQty(item);
   }

   public remove(item : CartItem) {
        this.shoppingService.removeItem(item);
   }

   public itemsValue() : number {
       return this.shoppingService.total();
   }

  public checkOrder(order: Order) : Observable<string> {
      return this.http.post<Order>(`${MEAT_API}/orders`, order)
                      .map(order => {
                          this.orderId = order.id;
                          return order.id;
                      });
  }

  public clear() : void {
      this.shoppingService.clear();
  }
}