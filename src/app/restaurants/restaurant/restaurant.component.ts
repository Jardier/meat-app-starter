import { Component, OnInit, Input } from '@angular/core';
import { Restaurant } from '../../shared/restaurant.model'

import { state, style, transition, animate, trigger } from '@angular/animations';

@Component({
  selector: 'mt-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css'],
  animations: [
   trigger('restaurantAppeared', [
     state('ready', style({
       opacity : 1
     })),
     transition('void => ready', [
       style({opacity: 0, transform: 'translate(-30px,-10px)'}),
       animate('300ms 0s ease-in-out')
     ])
   ])
  ]
})
export class RestaurantComponent implements OnInit {

  @Input()
  public restaurant : Restaurant;

  public restaurantState : string = 'ready';

  constructor() { }

  ngOnInit() {
  }

}
