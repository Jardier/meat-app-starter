import { Component, OnInit  } from '@angular/core';
import { Restaurant } from 'app/shared/restaurant.model';
import { RestaurantsService } from './restaurants.service';
import { trigger, state, style, transition, animate} from '@angular/animations';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/from';

@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.css'],
  animations: [
    trigger('toogleSearch',[
      state('hidden', style({
        opacity: 0,
        "max-height" : "0px"
      })),
      state('visible', style({
        opacity: 1,
        "max-height" : "70px",
        "margin-top" : "20px"
        
      })), 
      transition('*<=>*', animate('250ms 0s ease-in-out')),
      
    ])]
})
export class RestaurantsComponent implements OnInit {

  public restaurants : Restaurant [];

  public searchBarState : string = 'hidden';
  public searchForm : FormGroup;
  public searchControl : FormControl;

  constructor(private restaurantsService: RestaurantsService
            , private formBuilder : FormBuilder
    ) {}

  ngOnInit() {
     this.searchControl = this.formBuilder.control('');
     this.searchForm = this.formBuilder.group({
       searchControl : this.searchControl
     })
     this.searchControl.valueChanges
                       .debounceTime(500)
                       .distinctUntilChanged()
                       .switchMap(searTerm => 
                            this.restaurantsService
                                .restaurants(searTerm)
                                .catch(error => Observable.from([])))
                       .subscribe(restaurants => this.restaurants = restaurants);

     this.restaurantsService
         .restaurants()
         .subscribe((restaurants) => {
           this.restaurants = restaurants
         });
  }

  public toogleSearch() : void {
    this.searchBarState = this.searchBarState === 'hidden' ? 'visible' : 'hidden';
  }

}
