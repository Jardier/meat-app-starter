import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch'
import { Observable } from 'rxjs/Observable';

import { Restaurant } from "app/shared/restaurant.model";
import { MEAT_API } from "app/app.api";
import { ErrorHendler } from 'app/app.error-handler';
import { Review } from "app/shared/review.model";
import { MenuItem } from "app/shared/menu-item.model";

@Injectable()
export class RestaurantsService {

    constructor(private http : HttpClient) {}

    public restaurants(search? : string ) : Observable<Restaurant[]> {
        let params : HttpParams = undefined;

        if(search) {
            params = new HttpParams().append('q', search);
        }

        return this.http
                   .get<Restaurant[]>(`${MEAT_API}/restaurants`, {params: params });
                  
    }

    public restaurantById(id : string) : Observable<Restaurant> {
        return this.http
                   .get<Restaurant>(`${MEAT_API}/restaurants/${id}`)
                  
    }

    public reviewsOfRestaurant(id : string) : Observable<Review> {
        return this.http
                   .get<Review>(`${MEAT_API}/restaurants/${id}/reviews`)
                  
              
    }

    public menuOfRestaurant(id : string) : Observable<MenuItem[]> {
        return this.http
                   .get<MenuItem[]>(`${MEAT_API}/restaurants/${id}/menu`);
                 
    }
}