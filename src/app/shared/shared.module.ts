import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { InputComponent } from "./input/input.component";
import { RadioComponent } from "./radio/radio.component";
import { RatingComponent } from "./rating/rating.component";
import { SnakbarComponent } from './messages/snakbar/snakbar.component';

import { RestaurantsService } from "app/restaurants/restaurants.service";
import { ShoppingCartService } from "app/restaurant-detail/shopping-cart/shopping-cart.service";
import { OrderCompraService } from "app/order-compra/order-compra.service";
import { NotificationService } from "app/shared/messages/notification.service";

@NgModule({
    declarations : [InputComponent, RadioComponent, RatingComponent, SnakbarComponent],
    imports : [FormsModule, ReactiveFormsModule, CommonModule],
    exports: [InputComponent, RadioComponent, RatingComponent,
              FormsModule, ReactiveFormsModule, CommonModule, SnakbarComponent]
})
export class SharedModule {

    public static forRoot() : ModuleWithProviders {
        return {
            ngModule : SharedModule,
            providers: [RestaurantsService, ShoppingCartService,
                        OrderCompraService, NotificationService]
        }
    }

}