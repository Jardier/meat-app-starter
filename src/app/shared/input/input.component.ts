import { Component, OnInit, Input, ContentChild, AfterContentInit } from '@angular/core';
import { NgModel, FormControlName } from '@angular/forms';

@Component({
  selector: 'mt-input-component',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit, AfterContentInit {

  input : any;

  @Input()
  label: string;
  @Input()
  errorMessage: string;

  @ContentChild(NgModel) 
  model : NgModel;

  @ContentChild(FormControlName)
  public control : FormControlName;

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentInit() {
    this.input = this.model || this.control;
    if(this.input === undefined) {
      throw new Error("Esse componente precisa ser usado com uma Diretiva ngModel ou FormControlName");
    }
  }

  public hasSuccess() : boolean {
    return this.input.valid && (this.input.dirty || this.input.touched);
  }

  public hasError() : boolean {
    return this.input.invalid && (this.input.dirty || this.input.touched);
  }

}
