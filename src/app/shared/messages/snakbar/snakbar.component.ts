import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { NotificationService } from '../notification.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';
import { timer } from 'rxjs/observable/timer';


@Component({
  selector: 'mt-snakbar',
  templateUrl: './snakbar.component.html',
  styleUrls: ['./snakbar.component.css'],
  animations : [
    trigger('snak-visibility',[
      state('hidden', style({
        opacity : 0,
        bottom: '0px'
      })),
      state('visible', style({
        opacity: 1,
        bottom: '30px'
      })),
      transition('hidden => visible', animate('500ms 0s ease-in')),
      transition('visible => hidden', animate('500ms 0s ease-out'))
    ]),    
  ]
})
export class SnakbarComponent implements OnInit {

  public message : string = "Hello SnakBar!";
  public snakVisible : string = 'hidden'; 

  constructor(
    private notificationService : NotificationService
  ){}

  ngOnInit() {
    this.notificationService.notifier
        .do((message) => {
          this.message = message;
          this.snakVisible = 'visible';
        })
        .switchMap((message) => Observable.timer(3000))
        .subscribe((timer) => {
          this.snakVisible = 'hidden';
        })
        
  }

}
