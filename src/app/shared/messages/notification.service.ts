import { EventEmitter } from "@angular/core";

export class NotificationService {
    public notifier : EventEmitter<any> = new EventEmitter<any>();

    public notify(message : string) : void {
        this.notifier.emit(message);
    }
}