class Order {

    public constructor(
        public address: string,
        public number : number,
        public optionalAddress: string,
        public paymentOption: string,
        public orderItem: OrderItem[] = [],
        public id?: string
    ){}

}
class OrderItem {

    public constructor(
        public quantity: number ,
        public menuId : string
    ) {}
}
export {Order, OrderItem}